package com.techu.entregable2.datos;

import com.techu.entregable2.modelo.ModeloProducto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<ModeloProducto, String> {}

