package com.techu.entregable2.controlador;

import com.techu.entregable2.modelo.ModeloProducto;
import com.techu.entregable2.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/productos")
public class ControladorProducto {
    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping("/")
    public List<ModeloProducto> getProductos(){
        return servicioProducto.findAll();
    }

    @GetMapping("/{id}")
    public Optional<ModeloProducto> getProductoId(@PathVariable String id){
        return servicioProducto.findById(id);
    }

    @PostMapping("/")
    public ModeloProducto postProductos(@RequestBody ModeloProducto newProducto){
        servicioProducto.save(newProducto);
        return newProducto;
    }

    @PutMapping("/")
    public void putProductos(@RequestBody ModeloProducto productoToUpdate){
        servicioProducto.save(productoToUpdate);
    }

    @DeleteMapping("/")
    public boolean deleteProductos(@RequestBody ModeloProducto productoToDelete){
        return servicioProducto.delete(productoToDelete);
    }
}
