package com.techu.entregable2.servicio;

import com.techu.entregable2.datos.RepositorioProducto;
import com.techu.entregable2.modelo.ModeloProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProducto {

    @Autowired
    RepositorioProducto repositorioProducto;

    public List<ModeloProducto> findAll(){
        return repositorioProducto.findAll();
    }

    public Optional<ModeloProducto> findById(String id){
        return repositorioProducto.findById(id);
    }

    public ModeloProducto save(ModeloProducto entity){
        return repositorioProducto.save(entity);
    }

    public boolean delete(ModeloProducto entity){
        try{
            repositorioProducto.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
